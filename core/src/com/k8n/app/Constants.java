package com.k8n.app;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by 915 on 12.12.2014.
 */
public class Constants {

    public static final int WORLD_WIDTH = 200;
    public static final int WORLD_HEIGHT = 100;

    public static final Color notEmptyColor = new Color(0, 0, 0, 1);

    public static final Vector2 center = new Vector2(WORLD_WIDTH / 2, WORLD_HEIGHT / 2);
    public static int mapOffset = 1;
}

