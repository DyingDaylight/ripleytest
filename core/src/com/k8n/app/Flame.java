package com.k8n.app;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by 915 on 19.12.2014.
 */
public class Flame implements Disposable, Pool.Poolable {

    public static final int LIFETIME = 1;

    int width = 10;
    int height = 10;

    public int x = 0;
    public int y = 0;

    private float age = 0;

    Texture texture;

    public boolean alive;

    public Flame(){
        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        pixmap.setColor(0.8f, 0.3f, 0.1f, 1);
        pixmap.fill();
        texture = new Texture(pixmap);
        pixmap.dispose();
        alive = false;
    }

    public void init(int x, int y){
        this.x = x;
        this.y = y;
        age = 0;
        alive = true;
    }

    public void update(float delta) {
        age += delta;

        if (age > LIFETIME)
            alive = false;
    }

    @Override
    public void dispose() {
        texture.dispose();
    }

    @Override
    public void reset() {
        alive = false;
    }

    public void draw(SpriteBatch batch) {
        batch.draw(texture, x, y);
    }
}
