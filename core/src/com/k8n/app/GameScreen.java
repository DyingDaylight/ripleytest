package com.k8n.app;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

import java.util.Iterator;

/**
 * Created by 915 on 12.12.2014.
 */
public class GameScreen implements Screen, GameEventsListener {

    SpriteBatch batch;

    Ripley ripley;

    Pixmap backgroundPixmap;
    Texture backgroundTexture;

    OrthographicCamera camera;

    Player player;

    Array<Flame> activeFlames = new Array<Flame>();
    Pool<Flame> flamesPool = new Pool<Flame>() {

        @Override
        protected Flame newObject() {
            return new Flame();
        }
    };

    public GameScreen(SpriteBatch batch) {
        this.batch = batch;
        ripley = new Ripley();
        player = new Player();
        player.setListener(this);

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT);

        Gdx.input.setInputProcessor(inputAdapter);

        backgroundPixmap = new Pixmap(Gdx.files.internal("map.png"));
        backgroundTexture = new Texture(backgroundPixmap);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0.8f, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

       // ripley.update(delta);
        player.update(delta, ripley);
        updateFlames(delta);

        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        batch.draw(backgroundTexture, -1, -1);
       // batch.draw(ripley.ripleyTexture, 0, 0);
        drawFlames(batch);
        player.draw(batch);
        batch.end();
    }

    private void drawFlames(SpriteBatch batch) {
        Iterator<Flame> flameIterator = activeFlames.iterator();
        while (flameIterator.hasNext()) {
            Flame flame1 = flameIterator.next();
            if (flame1.alive) {
                flame1.draw(batch);
            }
        }
    }


    public void updateFlames(float delta) {
        Iterator<Flame> flameIterator = activeFlames.iterator();
        while (flameIterator.hasNext()) {
            Flame flame1 = flameIterator.next();
            flame1.update(delta);
            if (!flame1.alive) {
              //  ripley.isBurning(flame1);
                flameIterator.remove();
                flamesPool.free(flame1);
            }
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        player.dispose();
        ripley.dispose();
        backgroundPixmap.dispose();
        backgroundTexture.dispose();
    }

    InputAdapter inputAdapter = new InputAdapter(){

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            if (button == Input.Buttons.LEFT) {
                player.openFire();

                Vector3 vector3 = new Vector3(screenX, screenY, 0);
                camera.unproject(vector3);
                player.setFire((int) vector3.x, (int) vector3.y);
            }
            return super.touchDown(screenX, screenY, pointer, button);
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            if (button == Input.Buttons.LEFT){
                player.stopFire();
            }
            return super.touchUp(screenX, screenY, pointer, button);
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            if (player.isFiring()) {
                Vector3 vector3 = new Vector3(screenX, screenY, 0);
                camera.unproject(vector3);
                player.setFire((int) vector3.x, (int) vector3.y);
            }
            return super.touchDragged(screenX, screenY, pointer);
        }
    };

    private void createFire(Vector2 vector2) {
        Flame flame = flamesPool.obtain();
        flame.init((int) vector2.x, (int) vector2.y);
        activeFlames.add(flame);
    }

    @Override
    public void throwFlame(int x, int y) {
        createFire(new Vector2(x, y));
    }

    @Override
    public int getCellAvailable(int x, int y) {
        return backgroundPixmap.getPixel(x + Constants.mapOffset, y + Constants.mapOffset);
    }
}
