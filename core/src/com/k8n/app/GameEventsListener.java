package com.k8n.app;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by 915 on 22.12.2014.
 */
public interface GameEventsListener {

    public void throwFlame(int x, int y);
    public int getCellAvailable(int x, int y);
}
