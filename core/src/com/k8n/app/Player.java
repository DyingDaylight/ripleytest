package com.k8n.app;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by 915 on 18.12.2014.
 */
public class Player implements Disposable {

    public static final String LEFT = "LEFT";
    public static final String RIGHT = "RIGHT";
    public static final String UP = "UP";
    public static final String DOWN= "DOWN";

    private int speed = 3;

    private int width = 10;
    private int height = 10;

    private int x = Constants.WORLD_WIDTH / 2;
    private int y = Constants.WORLD_HEIGHT / 2;

    boolean isFiring = false;

    Flamethrower flamethrower;

    Texture texture;

    GameEventsListener listener;

    HashMap<String, Array<Vector2>> collisionPoints = new HashMap<String, Array<Vector2>>();

    Color color = new Color();

    public Player() {
        flamethrower = new Flamethrower();
        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        pixmap.setColor(0.2f, 0.1f, 0.8f, 1);
        pixmap.fill();
        texture = new Texture(pixmap);
        pixmap.dispose();

        Array<Vector2> leftVectors = new Array<Vector2>();
        leftVectors.add(new Vector2(0, height));
        leftVectors.add(new Vector2(0, 0));
        collisionPoints.put(LEFT, leftVectors);

        Array<Vector2> rightVectors = new Array<Vector2>();
        rightVectors.add(new Vector2(width, height));
        rightVectors.add(new Vector2(width, 0));
        collisionPoints.put(RIGHT, rightVectors);

        Array<Vector2> upVectors = new Array<Vector2>();
        upVectors.add(new Vector2(0, height));
        upVectors.add(new Vector2(width, height));
        collisionPoints.put(UP, upVectors);

        Array<Vector2> downVectors = new Array<Vector2>();
        downVectors.add(new Vector2(0, 0));
        downVectors.add(new Vector2(width, 0));
        collisionPoints.put(DOWN, downVectors);
    }

    public void setListener(GameEventsListener listener){
        this.listener = listener;
        flamethrower.setListener(listener);
    }

    public void openFire() {
        isFiring = true;
    }

    public void stopFire() {
        isFiring = false;
    }

    public void setFire(int x, int y) {
        flamethrower.findAim(new Vector2(this.x, this.y), new Vector2(x, y));
    }

    public boolean isFiring() {
        return isFiring;
    }

    public void draw(SpriteBatch batch) {
        batch.draw(texture, x, y);
        if (isFiring) {
            batch.draw(flamethrower.texture, flamethrower.x, flamethrower.y);
        }
    }

    public void update(float delta, Ripley ripley) {
        boolean hasMoved = false;
        int key = 0;
        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            hasMoved = true;
            key = Input.Keys.W;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            hasMoved = true;
            key = Input.Keys.S;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            hasMoved = true;
            key = Input.Keys.A;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            hasMoved = true;
            key = Input.Keys.D;
        }

        if (hasMoved) {
            testCollision(key);
        }

        if (isFiring&& hasMoved) {
            flamethrower.updateFire(x, y);
        }
    }

    @Override
    public void dispose() {
        flamethrower.dispose();
    }

    private boolean testCollision(int dir) {
        String direction = "";
        int delta = 0;
        Vector2 multipliers = new Vector2(0, 0);
        Vector2 increaseBy = new Vector2(0, 0);

        switch (dir) {
            case Input.Keys.W:
                direction = UP;
                delta = - height + 1;
                multipliers = new Vector2(0, 1);
                increaseBy = new Vector2(0, -1);
                break;
            case Input.Keys.S:
                direction = DOWN;
                delta = 0;
                multipliers = new Vector2(0, 1);
                increaseBy = new Vector2(0, 1);
                break;
            case Input.Keys.A:
                direction = LEFT;
                delta = 0;
                multipliers = new Vector2(1, 0);
                increaseBy = new Vector2(1, 0);
                break;
            case Input.Keys.D:
                direction = RIGHT;
                delta = - height + 1;
                multipliers = new Vector2(1, 0);
                increaseBy = new Vector2(-1, 0);
        }
        return checkCollision(direction, increaseBy, delta, multipliers);
    }

    private boolean checkCollision(String direction, Vector2 increaseBy, int delta, Vector2 multipliers) {
        Array<Vector2> sideVertices = collisionPoints.get(direction);

        for (Vector2 vertex : sideVertices) {
            color.set(listener.getCellAvailable((int) (vertex.x + this.x + speed),
                    (int) (vertex.y + this.y + speed)));
            if (color.equals(Color.BLACK)){
                int y = (int) vertex.y;
                int x = (int) vertex.x;
                while ((vertex.equals(Color.BLACK))) {
                    x = (int) (x + increaseBy.x);
                    y = (int) (y + increaseBy.y);
                    color.set(listener.getCellAvailable(x, y));
                };
                this.x = (x + delta) * (int) multipliers.x;
                this.y = (y + delta) * (int) multipliers.y;
                return false;
            }
        }
        this.x = this.x + (int) (speed * multipliers.x * increaseBy.x * -1);
        this.y = this.y + (int) (speed * multipliers.y * increaseBy.y * -1);
        return true;
    }
}
