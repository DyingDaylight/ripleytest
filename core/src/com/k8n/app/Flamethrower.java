package com.k8n.app;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by 915 on 18.12.2014.
 */
public class Flamethrower implements Disposable {

    private static final float MOVE_DISTANCE = 50f;
    int width = 10;
    int height = 10;

    int x = 0;
    int y = 0;

    Pixmap pixmap;
    Texture texture;
    private GameEventsListener listener;

    public Flamethrower(){
        pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        pixmap.setColor(0, 0, 0, 1);
        pixmap.fill();
        pixmap.setColor(1, 1, 1, 1);
        pixmap.drawPixel(0, 0);
        pixmap.drawPixel(width - 1, 0);
        pixmap.drawPixel(width - 1, height - 1);
        pixmap.drawPixel(0, height - 1);
        texture = new Texture(pixmap);
        pixmap.dispose();
    }

    @Override
    public void dispose() {
        texture.dispose();
    }

    public void findAim(Vector2 player, Vector2 aim) {
        float diff = (float) Math.sqrt(Math.pow(player.x - aim.x, 2) + Math.pow(player.y - aim.y, 2));

        if (diff < MOVE_DISTANCE) {
            setCoordinates((int) aim.x, (int) aim.y);
            return;
        }

        int newX = Math.round(player.x + MOVE_DISTANCE / diff * (aim.x - player.x));

        int newY = (int) (player.y + MOVE_DISTANCE / diff * (aim.y - player.y));

        if (newY <= y)
            newY = (int) Math.ceil(newY);
        else
            newY = (int) Math.floor(newY);
        setCoordinates(newX, newY);
    }

    private void setCoordinates(int newX, int newY) {
        x = newX;
        y = newY;
        if (listener != null) listener.throwFlame(x, y);
    }

    public void updateFire(int x, int y) {
        float diff = (float) Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2));

        if (diff < MOVE_DISTANCE) {
            setCoordinates(this.x, this.y);
            return;
        }

        int newX = Math.round(x + MOVE_DISTANCE / diff * (this.x - x));

        int newY = (int) (y + MOVE_DISTANCE / diff * (this.y - y));

        if (newY <= y)
            newY = (int) Math.ceil(newY);
        else
            newY = (int) Math.floor(newY);
        setCoordinates(newX, newY);
    }

    public void setListener(GameEventsListener listener) {
        this.listener = listener;
    }
}
