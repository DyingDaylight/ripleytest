package com.k8n.app;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectSet;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.plaf.synth.ColorType;

/**
 * Created by 915 on 12.12.2014.
 */
public class Ripley implements Disposable{

    private static final float UPDATE_TIME = 0.1f;
    private static final int MOVE_DISTANCE = 1;
    private static final int SCALE = 5;

    private Pixmap ripleyPixmap;
    public Texture ripleyTexture;

    private Array<Vector2> edgePoints = new Array<Vector2>();
    private Set<Integer> movingPoints = new TreeSet<Integer>();
    private Color tmpColor = new Color();

    private float lastUpdate = 0;

    private final Vector2 center;

    public Ripley() {
        center = new Vector2(scaledValue(Constants.WORLD_WIDTH) / 2, scaledValue(Constants.WORLD_HEIGHT) / 2);
        ripleyPixmap = new Pixmap(Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT, Pixmap.Format.RGBA8888);
        createNewRipley();
    }

    private void createNewRipley(){

        lastUpdate = 0;
        edgePoints.clear();

        int tmpX = 0;
        int tmpY = 0;

        ripleyPixmap.setColor(1, 1, 1, 1);
        ripleyPixmap.fillRectangle(0, 0, Constants.WORLD_WIDTH, Constants.WORLD_HEIGHT);
        ripleyPixmap.setColor(Constants.notEmptyColor);

        for (int x = 0; x < scaledValue(Constants.WORLD_WIDTH); x++) {
            edgePoints.add(new Vector2(x, tmpY));
            ripleyPixmap.fillRectangle(realValue(x), realValue(tmpY), SCALE, SCALE);
            tmpX = x;
        }

        for (int y = 1; y < scaledValue(Constants.WORLD_HEIGHT); y++) {
            edgePoints.add(new Vector2(tmpX, y));
            ripleyPixmap.fillRectangle(realValue(tmpX), realValue(y), SCALE, SCALE);
            tmpY = y;
        }

        for (int x = tmpX - 1; x > 0; --x) {
            edgePoints.add(new Vector2(x, tmpY));
            ripleyPixmap.fillRectangle(realValue(x), realValue(tmpY), SCALE, SCALE);
        }

        for (int y = tmpY; y > 0; --y) {
            edgePoints.add(new Vector2(0, y));
            ripleyPixmap.fillRectangle(0, realValue(y), SCALE, SCALE);
        }

        updateTexture();
    }

    public void update(float delta) {
        lastUpdate += delta;

        if (lastUpdate >= UPDATE_TIME) {
            moveForward();
            lastUpdate = 0;
        }
    }

    public void isBurning(Flame flame) {
        ripleyPixmap.setColor(1, 1, 1, 1);
        ripleyPixmap.fillRectangle(flame.x, flame.y, flame.width, flame.height);
        updateTexture();

        ObjectSet<Vector2> squaresOnFire = new ObjectSet<Vector2>();

        for (int x = 0; x < scaledValue(flame.width); x++) {
            for (int y = 0; y < scaledValue(flame.height); y++) {

                int i = scaledValue(realValue(x) + flame.x);
                int j = scaledValue(realValue(y) + flame.y);
                squaresOnFire.add(new Vector2(i, j));

                j = scaledValue(realValue(y) + flame.y + SCALE - 1);
                squaresOnFire.add(new Vector2(i, j));

                i = scaledValue(realValue(x) + flame.x + SCALE - 1);
                squaresOnFire.add(new Vector2(i, j));

                j = scaledValue(realValue(y) + flame.y);
                squaresOnFire.add(new Vector2(i, j));


            }
        }

        int rows = scaledValue(ripleyPixmap.getWidth());
        int col = scaledValue(ripleyPixmap.getHeight());

        for (Vector2 point : squaresOnFire) {
            int i = Math.round(point.x);
            int j = Math.round(point.y);

            if (i == 0 || j == 0) {
                Vector2 point2 = new Vector2(i, j);
                if (!isPointOnEdge(point2)) {
                    edgePoints.add(point2);
                }
            }

            for (int index = i - 1; index <= i + 1; index ++){
                for (int k = -1; k < 2; k++) {
                    if (index % rows >= 0 && ((j + k) % col) >= 0) {
                        if (isFullBlack(index, j + k)){
                            Vector2 point2 = new Vector2(index, j + k);
                            if (!isPointOnEdge(point2)) {
                                edgePoints.add(point2);
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean isFullBlack(int x, int y) {

        int i = realValue(x);
        int j = realValue(y);
        tmpColor.set(ripleyPixmap.getPixel(i, j));
        if (tmpColor.equals(Color.WHITE))
            return false;

        i = realValue(x) + SCALE - 1;
        j = realValue(y);
        tmpColor.set(ripleyPixmap.getPixel(i, j));
        if (tmpColor.equals(Color.WHITE))
            return false;

        i = realValue(x) + SCALE - 1;
        j = realValue(y) + SCALE - 1;
        tmpColor.set(ripleyPixmap.getPixel(i, j));
        if (tmpColor.equals(Color.WHITE))
            return false;

        i = realValue(x);
        j = realValue(y) + SCALE - 1;
        tmpColor.set(ripleyPixmap.getPixel(i, j));
        if (tmpColor.equals(Color.WHITE))
            return false;

        return true;
    }

    private void moveForward() {
        if (edgePoints.size == 0)
            return;

        movingPoints.clear();
        int movingPointsNum = MathUtils.random(0, 50);
        for (int i = 0; i < movingPointsNum; i++) {
            movingPoints.add(MathUtils.random(0, edgePoints.size - 1));
        }

        ripleyPixmap.setColor(0, 0, 0, 1);
        for (Integer index : movingPoints){
            Vector2 point = edgePoints.get(index);

            if (point.x == 0 || point. y == 0) {
                if (!isFullBlack((int) point.x, (int) point.y)) {
                    ripleyPixmap.fillRectangle(realValue(Math.round(point.x)), realValue(Math.round(point.y)), SCALE, SCALE);
                    continue;
                }
            }

            float diff = (float) Math.sqrt(Math.pow(point.x - center.x, 2) + Math.pow(point.y - center.y, 2));
            point.x = Math.round(point.x + MOVE_DISTANCE / diff * (center.x - point.x));

            float newY = point.y + MOVE_DISTANCE / diff * (center.y - point.y);

            if (newY <= center.y)
                point.y = (float) Math.ceil(newY);
            else
                point.y = (float) Math.floor(newY);

            ripleyPixmap.fillRectangle(Math.round(point.x) * SCALE, Math.round(point.y) * SCALE, SCALE, SCALE);

            if (!isPointOnEdge(point)) {
                edgePoints.add(point);
            }
        }
        updateTexture();

        Iterator<Vector2> edgeIterator = edgePoints.iterator();
        while (edgeIterator.hasNext()) {
            Vector2 point = edgeIterator.next();
            if (isSurrounded(point)){
                edgeIterator.remove();
            }
        }
    }

    private boolean isSurrounded(Vector2 point) {
        int rows = ripleyPixmap.getWidth() / SCALE;
        int col = ripleyPixmap.getHeight() / SCALE;

        int i = Math.round(point.x);
        int j = Math.round(point.y);

        if (!isFullBlack(i, j))
            return false;

        for (int index = i - 1; index <= i + 1; index ++){
            for (int k = -1; k < 2; k++) {
                if (index % rows > 0 && ((j + k) % col) > 0) {
                    if (!isFullBlack(index, j + k))
                        return false;
                }
            }
        }
        return true;
    }

    @Override
    public void dispose() {
        ripleyPixmap.dispose();
        ripleyTexture.dispose();
    }

    public boolean isPointOnEdge (Vector2 point) {
        int i = edgePoints.size - 1;
        while (i >= 0) {
            Vector2 vector2 = edgePoints.get(i--);
            if ( Math.round(point.x) ==  Math.round(vector2.x) &&  Math.round(point.y) ==  Math.round(vector2.y)) return true;
        }
        return false;
    }

    private int scaledValue(int i) {
        return  i / SCALE;
    }

    private int realValue(int i) {
        return i * SCALE;
    }

    private void updateTexture(){
        if (ripleyTexture != null)
            ripleyTexture.dispose();
        Pixmap flippedPixmap = flipPixmap(ripleyPixmap);
        ripleyTexture = new Texture(flippedPixmap);
        flippedPixmap.dispose();
    }

    public Pixmap flipPixmap(Pixmap src) {
        final int width = src.getWidth();
        final int height = src.getHeight();
        Pixmap flipped = new Pixmap(width, height, src.getFormat());

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                flipped.drawPixel(x, y, src.getPixel(x, height - y - 1));
            }
        }
        return flipped;
    }

    public int getColorAt(int x, int y) {
        return ripleyPixmap.getPixel(x + Constants.mapOffset, y + Constants.mapOffset);
    }
}
